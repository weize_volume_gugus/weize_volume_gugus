\section{Baseline}\label{sec:baseline}

Prior work in the field of food volume estimation was
done for example with the use of specialized equipment
such as depth images take by well calibrated
depth cameras for example \cite{antona_single_2020} and
\cite{sari_measuring_2023}.
Further work was done using multi views and
computer vision algorithms rather than neural networks
\cite{dehais_two-view_2017}.
An method very close to our approach
as implemented by \cite{lo_food_2018}.
They reconstructed the volume of RGB images from
depth estimations.
Another approach
is to estimate the food volume based on a preselected
geometrical shape \cite{xu_image-based_2013}, \cite{walker_measuring_2012}.

Given, that we only have simple RGB images, and the results
of the depth map estimation method seemed suffer from high errors,
the last method seems to fit the best as baseline for our approach.
Moreover wheat spikes have similar geometrical shapes.
As the results in the two previously mentioned papers on food estimation
based on geometrical shape only include very little experiments
and data to compare with, we implemented our own baseline.

Based on the assumption that a wheat spikes cross-section is circular,
we developed a baseline algorithm which computes cylindrical on small
intervals along the spike.

The algorithm to achieve the computation of such a model
can be divided in to four steps:
\begin{enumerate}
    \item Segment the spike, i.e. get mask.
    \item Compute the skeleton and main axis of the mask.
    \item Fit two parametrized smoothing splines along the main axis.
    \item Compute orthogonal (to the spline) width of spline mask
        at arbitrary points.
    \item Integrate over the parametrized spline to get the volume.
\end{enumerate}


\subsection{Spike Segmentation}
\label{spike_segment}

Similar to the bars in the preprocessing, the task here
is to recognize wheat spikes in the images in which the
bars were previously removed.

The same problems as in the preprocessing due to different
light settings rule out the direct application of color based
segmentation. Again we rely on segment anything by
\cite{kirillov_segment_2023} for the segmentation.

As the preprocessing and this task are closely related, we can
reuse the code here. The only difference is the
points selected to determine which object has to be segmented.
For simplicity we implemented a "semi" abstract class for
the segmentation in our setting, such that we only needed
to adapt the algorithm selecting the points. In this
case we still rely on the colors here but in this case it
works as it is possible to select only extremal points in
one color.

\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[angle=90,width=\columnwidth]{figures/21_15_6_3_segmented}}
\caption{Segmentation of spike in image (21\_15\_6\_3.jpg).}
\label{fig:segmented_spike}
\end{center}
\vskip -0.2in
\end{figure}

\subsection{Compute Skeleton / Main Axis}

To generate the skeleton on the segmentation mask, we use
a thinning algorithm algorithm by \cite{zhang_fast_1984}.
This algorithm creates a skeleton of the segmentation
mask.
\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[angle=90,width=\columnwidth]{figures/21_15_4_5_skeleton}}
\caption{Skeleton of the spike in (21\_15\_4\_5.jpg). The red line depicts
the skeleton.}
\label{fig:skeleton}
\end{center}
\vskip -0.2in
\end{figure}

Having this skeleton, we want to reduce it to the main axis, i.e. the longest
path possible. For this we in principle use \cref{alg:main_axis}.

\begin{algorithm}[tb]
   \caption{Find\_Main\_Axis}
   \label{alg:main_axis}
\begin{algorithmic}
   \STATE {\bfseries Input:} skeleton pixel mask $S$, first point $p$.
   \STATE Set $path := [p]$
   \STATE Delete first point from $S$
   \STATE Delete $N(p)$ from $S$
   \STATE Set $subpaths := []$
   \FOR{$\widetilde{p} \in N(p)$}
   \STATE Set $tmp$ := {\bfseries Find\_Main\_Axis}($S$, $\widetilde{p}$)
   \STATE $subpaths$ append $tmp$
   \ENDFOR
   \STATE $maxpath := \text{argmax}_{z \in subpaths} \text{length}(z)$
   \STATE Set $path = \text{concatenate}(path, maxpath)$
   \STATE Return $path$
\end{algorithmic}
\end{algorithm}
In \cref{alg:main_axis} $N(p)$ denotes the neighbours of the point
$p$ in  $S$. That is
for the point $p = (i,k)$ we have  $N(p) = \{\widetilde{p}:
\widetilde{p} \in (i\pm 1, k \pm 1), p \in S\}$.
We point out that, for efficiency reasons,
the implemented algorithm only recurses if $|N(p)| > 1$, otherwise
it just adds the next neighbour to the path in a loop. Moreover we hid some
details for the definition of the set of neighbours because in reality
there are some issue with the connectivity.
Lastly, the finding the first points i.e. the entry points of the
algorithm is non trivial, and needed some more tricks. These
change do not change the idea of the code shown in \cref{alg:main_axis}.

After applying \cref{alg:main_axis} on the skeleton in \cref{fig:skeleton}
we get the main axis depicted in \cref{fig:main_axis}.

\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[angle=90,width=\columnwidth]{figures/21_15_4_5_axis}}
\caption{Main axis of the spike in (21\_15\_4\_5.jpg). After applying
\cref{alg:main_axis} on the skeleton in \cref{fig:skeleton}.}
\label{fig:main_axis}
\end{center}
\vskip -0.2in
\end{figure}

\subsection{Spline Fitting}

Using the main axis depicted in \cref{fig:main_axis} we then
fit two parametrized splines along the main axis. This
is, we solve the following regression problem twice:
\begin{align}
    \sum_{i=0}^{n-1}|z_i - f_z(t_i)|^2 + \lambda
    \int_{0}^{1}(f^{(2)}(u))^2 \, du.
    \label{eq:smooth_spline}
\end{align}
Where $n$ is the number of points we have, and we take
$z_i = x_i$ for the first fit, such that  $f_x(t)$ will
then become the spline fit of the  $x$-coordinate,
when moving along the main axis parametrized by  $t$.
Analogously  $f_y(t)$ will hence be the fit for the
$y$-coordinate.
For both fits $f_z(t)$  we get a B-Spline, which is
a piecewise polynomial of the form
\begin{align*}
    f_z(t) = \sum_{j=0}^{n-1} c_{j} B_{j}(t),
\end{align*}
where $c_j$ are the coefficients computed \cref{eq:smooth_spline}
and $B_j$ are the polynomial basis functions of degree  $3$
defined recursively using the node set $\{t_0, \dots, t_{n-1}\}$,
which is in our algorithm equispaced i.e. $t_i := \frac{i}{n-1}$,
\begin{align*}
    \widetilde{B}_{i,0}(t)
    &:= \begin{cases}
        1, & t_i \leq t < t_{i+1}\\
        0, & \text{otherwise}
    \end{cases},\\
    \widetilde{B}_{i,k+1}
    &:= \frac{t - t_i}{t_{i+k-1} - t_i} \widetilde{B}_{i, k}(t)
    + \frac{t_{i+k} - t}{t_{i+k} - t_{i+1}} \widetilde{B}_{i+1, k}(t),\\
    B_j(t)
    &:= \widetilde{B}_{j,3}.
\end{align*}
For the implementation details we relay on the library
scipy. The results of such a fit are depicted in \cref{fig:spline_curve}.

\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[angle=90,width=\columnwidth]{figures/21_15_4_5_curve}}
\caption{Fitted smoothing splines of the spike in (21\_15\_4\_5.jpg).
Based on points in \cref{fig:main_axis}.}
\label{fig:spline_curve}
\end{center}
\vskip -0.2in
\end{figure}

\subsection{Orthogonal mask width}

The main reason for the spline fit in the preceding section
is that a spline is continuously differentiable at any points.
So we can compute orthogonal vectors in the two dimensional
plane.

Using this, we can then implement a function returning the
width, orthogonal to the spline at every point of the mask.
This is best visualized in \cref{fig:mask_widths}, in which we computed
this width at 20 equidistant points along the curve and
at each point we visualized along which line the width is computed.
\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[angle=90,width=\columnwidth]{figures/21_15_4_5_widths}}
\caption{Widths computed orthogonally to the parametrized
    axis in (21\_15\_4\_5.jpg).
Based on the axis in \cref{fig:spline_curve}.}
\label{fig:mask_widths}
\end{center}
\vskip -0.2in
\end{figure}

\subsection{Model Volume}

From the previous subsections we have a parametrized curve of the
spikes main axis $g:[0,1] \to \mathbb{R}^2$ and a radius function
$r:[0,1] \to \mathbb{R}$, which gives the radius of the spike
at each point along along the curve  $g$.
So we can use this to estimate the volume when taking into account the
length $l$ of the spike which itself can also be computed along the
parametrized curve
\begin{align*}
    L
    &:= \int_{0}^{1} \|D_t g(u)\| \, du.
\end{align*}
The volume of our model is then defined by re-parametrization
\begin{align*}
    V
    &:= \int_{0}^{L} r\left(\frac{t}{L}\right) \, dt.
\end{align*}
We then approximate both integrals with a trapezoidal rule
and 600 equidistant intervals.
That is a basic trapezoidal rule. As a remark, we point out
that although continuously differentiable, the spline approximation
lacks of further smoothness properties. Therefore higher order
quadrature rules (i.e. gauss-quadrature of higher order)
does not converge in our case.

The resulting volume can also be visualized by sampling points
on the surface of the volume we compute and using meshing software
to generate a mesh. The result after such meshing is depicted in
\cref{fig:3dbaseline}

\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{figures/21_15_4_5_3d_baseline}}
\caption{Three dimensional reconstruction of the estimated volume
with the baseline method for spike image (21\_15\_4\_5.jpg).}
\label{fig:3dbaseline}
\end{center}
\vskip -0.2in
\end{figure}

As a final remark, we point out that the radius as well as
the length are originally measured with the reference unit
of pixels. That is, each pixel is one unit of length. To transform
this to reasonable values, we need to know the ratio of the image size
in pixels and the real-world length of the captured picture.
This can be done by passing a constant to the corresponding functions.
Moreover it is to mention that by it's design this baseline will overestimate
the volume. To account for this we might make a linear regression to find
account for this overestimation with the assumption of a linear dependency
between overestimated volume and actual volume.
