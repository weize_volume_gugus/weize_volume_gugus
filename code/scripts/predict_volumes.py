from PIL import Image
import os
import argparse
from lib.inferrer import Inferrer
from icecream import ic

def main():
    ic("hello")
    parser = argparse.ArgumentParser(description='Predicts volumes of crops based on image data')
    parser.add_argument('--image_path', type=str, required=True, help='Path to the folder containing images')
    parser.add_argument('--model_path', type=str, required=True, help='Path to the folder containing the pretrained model')
    parser.add_argument('--output_path', type=str, required=True, help='Path to the output folder')

    print("hello?")
    args = parser.parse_args()

    inferrer = Inferrer(image_file_or_folder_path=args.image_path, saved_model_path=args.model_path, output_path=args.output_path)
    inferrer.infer_pipeline()

if __name__ == "__main__":
    main()
