import click
from pathlib import Path
import pandas as pd
import numpy as np
from icecream import ic
import matplotlib.pyplot as plt
import scipy.stats as st
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from tueplots import bundles
import re 


def volume_statistics(data : pd.DataFrame, name : str, output_folder : Path):
    
    # group by plant based on image id
    id = data.index.str.extract(r'^(\d{1,2}_\d{1,2}_\d{1,2})', expand=False)
    data["p_id"] = id

    # get image stats
    mean_vol = data.groupby("p_id")["volume"].mean("vol")
    np_mean_vol = np.array(mean_vol)
    std_vol = data.groupby("p_id")["volume"].std()
    np_std_vol = np.array(std_vol)
    
    # compute basic statistics over plants
    n_plants = mean_vol.shape[0]
    mean = np.mean(np_mean_vol)
    variance = np.var(np_mean_vol)
    
    mask_bad = (mean_vol >= 8000) | (mean_vol < 1500)
    
    # plot histogram with gaussian fit
    print(f'Number of plants: {n_plants}')
    print(f'Mean volume: {mean}')
    print(f'Standard deviation in volume: {np.sqrt(variance)}')
    print(f'Number of bad points: {np.sum(mask_bad)}')
    
    x = np.linspace(np.min(np_mean_vol), np.max(np_mean_vol), 1000)
    norm_fit = st.norm.pdf(x, mean, np.sqrt(variance))

    ic(mean_vol[mask_bad])
    plt.hist(np_mean_vol, bins=100, density=True, color="green", label="Volume Distribution")
    plt.plot(x,norm_fit, '-', c="red", label="Fitted Normal")
    plt.legend(loc='best', frameon=False)
    plt.savefig(f'{output_folder}/distribution_{name}.jpg')
    
    

def compare(df_true : pd.DataFrame, df_estim : pd.DataFrame,  name : str, output_folder : Path, baseline : bool = False, scaling : bool = False):
    """Compute and print statistics

    Args:
        df_true (pd.DataFrame): True / measured volumes
        df_estim (pd.DataFrame): Estimated volumes
    """
    #if baseline results are used, remove the all volumes < 3000 where the segmentation failed
    

    # Check if the filename contains "baseline"
    if baseline == True:
        # Remove rows with values smaller than 3000 in the "volume" column because segmentation failed 
        df_estim = df_estim[df_estim['volume'] >= 3000]
       
    df_true_ids = df_true.index
    df_estim_ids = df_estim.index
    
    intersection_ids = set(df_true_ids) & set(df_estim_ids)
    ic(len(intersection_ids))
    
    print(f"Stats for estimations")
    print(f"Number of true images {df_true.shape[0]}")
    print(f"Number of estimated images {df_estim.shape[0]}")
    print(f"Images contained in both {len(intersection_ids)}")
    
    np_intersection_ids = np.array(intersection_ids)

    true_volumes = np.array(df_true.loc[np_intersection_ids,"volume"])
    ic(len(true_volumes))
    estim_volumes = np.array(df_estim.loc[np_intersection_ids,"volume"])
    ic(len(estim_volumes))
    diff_volumes = np.abs(estim_volumes - true_volumes)
    rel_diff_volumes = np.abs(estim_volumes - true_volumes) / true_volumes
    
    # Check if the filename contains "baseline" and apply scaling 
    if baseline == True:
        mean_baseline = np.mean(estim_volumes)
        mean_true = np.mean(true_volumes)
        ic(mean_baseline)
        ic(mean_true)
    
        #1)
        # Calculate the scaling factor using a logarithmic transformation
        #scaling_factor = np.log1p(estim_volumes.max()) / np.log1p(true_volumes.max())
        # Apply the scaling factor to the predicted values
        #estim_volumes = estim_volumes ** (1 / scaling_factor)
        
        #2)
        # Calculate the scaling factor using a square root transformation
        #scaling_factor = np.sqrt(estim_volumes.max() / true_volumes.max())
        # Apply the scaling factor to the predicted values
        #estim_volumes = estim_volumes ** (1 / scaling_factor)
        
        #3)
        #scaling_factor = mean_true/mean_baseline
        #estim_volumes = estim_volumes * scaling_factor
        
    #if scaling == True: 
        #mean_baseline = np.mean(estim_volumes)
        #mean_true = np.mean(true_volumes)
        #add a factor to improve the predictions  
        #scaling_factor = mean_true - mean_baseline
        #ic(scaling_factor)
        #scaling factor of best model of train set is ..., we will take this one for all models
        #estim_volumes = estim_volumes + 139.611725431183
        #ic(scaling_factor)
        
    mean_baseline = np.mean(estim_volumes)
    mean_true = np.mean(true_volumes)
    #add a factor to improve the predictions  
    scaling_factor = mean_true - mean_baseline
    ic(scaling_factor)
        
 
    
    # Calculate the mean
    #mean_y = np.mean(true_volumes)
    #MAE:
    #substract the mean of y of each predicted volume, and take the absolute value
    #abs = np.mean(np.abs(estim_volumes - mean_y))
    # Sum up the absolute values
    #sum_absolute_values = np.sum(abs)
    # Divide the sum by the number of elements
    #mean_absolute_value = sum_absolute_values / len(estim_volumes)
    
    MAE = mean_absolute_error(true_volumes, estim_volumes)
    ic(MAE)
        
    #MSE:
    MSE = mean_squared_error(true_volumes, estim_volumes)
    
    mse_test = np.mean((true_volumes-estim_volumes)**2)
    
    #RMSE:
    RMSE = np.sqrt(MSE)
    
    #MAPE:
    def mean_absolute_percentage_error(y_true, y_pred): 
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        mask = y_true != 0  # Avoid division by zero
        return np.mean(np.abs((y_true[mask] - y_pred[mask]) / y_true[mask])) * 100

    # Calculate MAPE
    MAPE = mean_absolute_percentage_error(true_volumes, estim_volumes)
        
    #R2:
    r_squared = r2_score(true_volumes, estim_volumes)
    
    #Dictionary with statistics:
    statistics = pd.DataFrame(columns=["metric", "value"])
    
    statistics.loc[0] = {"metric": "mean_error", "value": np.mean(diff_volumes)}
    statistics.loc[1] = {"metric": "max_error", "value": np.max(diff_volumes)}
    statistics.loc[2] = {"metric": "min_error", "value": np.min(diff_volumes)}
    statistics.loc[3] = {"metric": "std_error", "value": np.std(diff_volumes)}
    statistics.loc[4] = {"metric": "rel_mean_error", "value": np.mean(rel_diff_volumes)}
    statistics.loc[5] = {"metric": "rel_max_error", "value": np.max(rel_diff_volumes)}
    statistics.loc[6] = {"metric": "rel_min_error", "value": np.min(rel_diff_volumes)}
    statistics.loc[7] = {"metric": "rel_std_error", "value": np.std(rel_diff_volumes)}
    statistics.loc[8] = {"metric": "MAE", "value": MAE}
    statistics.loc[9] = {"metric": "MSE", "value": MSE}
    statistics.loc[10] = {"metric": "RMSE", "value": RMSE}
    statistics.loc[11] = {"metric": "MAPE", "value": MAPE} #“How much (what %) of the total variation in Y(target) is explained by the variation in X(regression line)”
    statistics.loc[12] = {"metric": "R2", "value": r_squared}
    
    # Save the DataFrame to a CSV file with the specified path and variable name
    statistics.to_csv(f'{output_folder}/{name}_vs_true_df.csv', index=False)
    
    setting = bundles.icml2022(usetex=False)
    plt.rcParams.update(setting)
    # Plotting the scatter plot
    plt.scatter(true_volumes, estim_volumes, color='black', label='True vs Predicted', marker='o', s=4)
    # Plotting the diagonal line (perfect predictions)
    plt.plot([min(true_volumes), max(true_volumes)], [min(true_volumes), max(true_volumes)], linestyle='--', color='red', label='Perfect Predictions')
    # Adding labels and title
    plt.xlabel('True Volumes [mm³]')
    plt.ylabel('Predicted Volumes [mm³]')
    plt.title(name)
    #plt.legend()
    # Adding R-squared value as text annotation
    plt.text(0.05, 0.9, f'R²: {r_squared:.2f}', transform=plt.gca().transAxes, color='black')

    # Display the plot
    plt.show()
    
    #name to save
    file_name = f'{output_folder}/scatter_plot_{name}.png'
    # Save the plot to a file (e.g., PNG, PDF, etc.)
    plt.savefig(file_name)
    
    for key in statistics.keys():
        print(f"{key} : {statistics[key]}")
        
        
        
    
@click.command()
@click.option("--volumes", type=str, required=True, help="Path to csv file containing measured volumes")
@click.option("--estimations", type=str, default=None, help="Path to csv file containing estimated volumes")
@click.option("--baseline", is_flag = True, type=bool, help="Add if one file is the baseline file")
@click.option("--scaling", is_flag = True, type=bool, help="Add scaling (add a constant)")
@click.option("--name", type=str, required=True, help="Name for image and statistic table to save")
@click.option("--output_folder", type=str, required=True, help="Folder in which statistics and images are saved")

def main(volumes : str, estimations : str | None, baseline : bool, scaling : bool, name : str, output_folder : str):
    volumes = Path(volumes)
    output_folder = Path(output_folder)
    if not volumes.exists():
        print(f"Error file {volumes} not found")
    
    if not estimations is None:
        estimations = Path(estimations)
        if not estimations.exists():
            print(f"Error file {estimations} not found")
        else:
            df_true = pd.read_csv(volumes, index_col="img_id")
            df_estim = pd.read_csv(estimations, index_col="img_id")
            compare(df_true=df_true, df_estim=df_estim, baseline=baseline, scaling=scaling, name=name, output_folder=output_folder)
    else:
        df_volumes = pd.read_csv(volumes, index_col="img_id")
        volume_statistics(df_volumes, name=name, output_folder=output_folder)
        
        

if __name__ == "__main__":
    #main(true_volumes="data/vol_mapping.csv", estimations="data/baseline_volumes.csv")
    main()