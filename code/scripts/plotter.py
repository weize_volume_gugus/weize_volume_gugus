import matplotlib.pyplot as plt
from tueplots import bundles, figsizes
import os
import torch
from icecream import ic

def plot(path: str, for_paper: bool):
    ic()

    if for_paper:
        plt.rcParams.update(bundles.icml2022(usetex=False))
    
    # Create a new folder called "plots" to save the figures
    plots_folder = os.path.join(path, "plots")
    os.makedirs(plots_folder, exist_ok=True)

    # Get a list of all files with .pth extension in the folder
    pth_files = [f for f in os.listdir(path) if f.endswith('.pth')]
    # Iterate through each .pth file and open it
    for pth_file in pth_files:
        ic(pth_file)

        file_path = os.path.join(path, pth_file)
        
        try:
            # Load the configuration of the PyTorch model from the .pth file
            config = torch.load(file_path, map_location=torch.device('cpu'))

            if for_paper:
                pass

            else:
                plt.figure(figsize=(10.0, 6.0))
                plt.title(f'Activation: {config["activation"]}, Dropout-Rate: {config["dropout_rate"]} Number of hidden Layers: {len(config["architecture"])}')

             

            train_loss = config['train_loss']
            val_loss = config['val_loss']
            ic(len(train_loss))
            ic(len(val_loss))

            num_epochs = config['epochs']
            epochs = list(range(1, num_epochs + 1))

            # Plotting the losses
            plt.plot(epochs, train_loss, label='Train Loss')
            plt.plot(epochs, val_loss, label='Validation Loss')

            plt.xlabel('Epochs')
            plt.ylabel('Loss')
            
            plt.legend()
            plt.show()

            # Save the figure in the "plots" folder with a unique name (e.g., using pth_file without extension)
            figure_name = os.path.splitext(pth_file)[0] + "_plot.png"
            figure_path = os.path.join(plots_folder, figure_name)
            plt.savefig(figure_path)

            
        except Exception as e:
            print(f"Error loading model from {file_path}: {e}")

if __name__ == "__main__":
    ic()
    plot("/home/aaronh/DeepLearning/weize_volume_gugus/code/data/images_no_bar_crop_splitRight_output/AllInnOne/", False)

