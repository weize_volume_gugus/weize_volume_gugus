# Code folder

We use [Poetry](https://python-poetry.org) to track dependencies.
Add dependencies with `poetry add <package>` to make sure it is
tracked. You can run the code in an environment according to the
`poetry.lock` file with `poetry run <file>`.

# Description of the project

Volume estimation of wheat spikes is an important task to improve yield to mitigate adverse impacts of climate change and the effects of an increasing world population. A dataset comprising about 970 wheat spikes volumes and corresponding images underwent preprocessing before being utilized for volume estimation. During the preprocessing stage, the cone needs to be removed from the scans in order to extract a precise volume. Furthermore, bars are removed from the images, the spikes are inpainted and segmented in order to increase accuracy in volume prediction and spikelet counting. 

Baseline model: 
We developed a baseline algorithm that computes cylindrical volumes at small intervalls along the spike. This baseline model was used to answer the question if neural networks perform better than a baseline to estimate the wheat spike volume.      

Neural networks: 
The dateset consists of images and scans of almost 970 wheat spikes. To gain more information, the spikes were imaged from 6 different angles. We augmented the dataset with artificial pictures of the 3D model in various perspective. This resulted in five different datasets, comprising real images, black and coloured artificial images, and a mixture of real and artificial images. These datasets were used to predict the volume using neural networks.

The dataset with the mixture of real and artificial images consisted of the typical real images along with 6 artificial images of the spikes. Our objective was to investigate whether augmenting the dataset using images generated from scans can improve the prediction accuracy. To address this questions, the dataset was split based on individual plants rather than images, preventing artificial images in the training set of a spike whose images is in the validation set, i.e. the validation set consists of real images, which have never been seen before (neither as real images nor as artificial ones). 

Depth estimations: 
A different approach to estimate the spike volume is to reconstruct the volume of RGB images from depth estimations. In this project, depth maps were constructed, but the extraction of the volume from depth maps was not completed within the project timeframe and will be conducted in a subsequent step. However, the code to estimate the depth maps can be found here: dpt-gen.py. 

Extension spikelet counting: 
The number of spikelets per spike determine the number of grains, and therefore yield. It is a cruicial trait and implementing the extraction of this trait in a high-throughput phenotyping workflow could hold great promis for improving selection. The code to estimate the number of spikelets per spike can be found in the script counting_spikelets_resnet.py. No results are presented in the report as the model was still a work in progress was not completed within the project timeframe. 


## Installation and setup

Make sure to download and install the necessary files:
- download [deepfill_model](https://drive.google.com/u/0/uc?id=1L63oBNVgz7xSb_3hGbUdkYW1IuRgMkCa&export=download) and copy the `.pth` file to the folder `code/data/models/deepfill/`

## Scripts

#### gen-dataset
This script can be executed in the poetry environment (entered with `poetry shell`) with e.g. the command:
```
gen-dataset -d data/Scans -o data/Scans_out -v 3 --outputsize 256 256
```
Which will generate 3 views of each spike by placing the camera around the spike in a circular fashion.
Note that with the flag `-s` or `--spherical` the camera can be placed anywhere around the spike.
For more information call `gen-dataset -h`.

#### rm-bars
This script can be executed in the poetry environment (entered with `poetry shell`) with e.g. the command:
```
rm-bars --input data/lab_images --img-output data/lab_images_no_bar --mask-output data/lab_images_mask
```
For each image in the `--input` folder, it will create a mask for the bars and store it in `--mask-output`,
moreover it will generate for each image a new image with the bars removed and inpainted.

For this to work it is important to download pretained weights for segmentation and imputing in the following locations:

Segment anything weights: [ViT-H SAM model](https://dl.fbaipublicfiles.com/segment_anything/sam_vit_h_4b8939.pth)
They have to be placed in `data/models/segment_anything/sam_vit_h_4b8939.pth`

Deepfill weights: [Deepfill Model](https://drive.google.com/u/0/uc?id=1L63oBNVgz7xSb_3hGbUdkYW1IuRgMkCa&export=download).
They have to be placed in `data/models/deep_fill/states_pt_places2.pth`

Note that the origninal weights are trained on a tensorflow model. We are using the weights converted by the reimplementation
in pytorch [original reimplementation](https://github.com/nipponjo/deepfillv2-pytorch). As this repostory is not set up for
installation, the actual code we then use in the script is forked form the latter repository and adapted for our use,
[fork of reimplementation](https://github.com/ncograf/deepfillv2-pytorch).

#### cut-spikes
This script can be executed in the poetry environment (entered with `poetry shell`) with e.g. the command:
```
cut-spikes --input-dir data/special_scans --output-dir data/special_scans_no_cone --plot-cone --type no_color
```
Use `cut-spikes --help` see all options.

The `--plot-cone` flag is to plot every single spike with the cone which will be removed.
There are more opions ot determine the cone height and radius.

Note that the script works for any type of scans, no matter, wheter the spike was fixed on the side of the block
or on top of it.

Type color needs to be set to color or no_color, depending on the ply files. If the color information is available, it will be re-added to the ply file if type is set to color


#### gen-base
This file is used to generate the base line volumes using some "cylindrical" fitting of the splines.
Input should be a directory with images (note that for this imput it is assumed that the
bars are already removed) and output will then be a the masked spikes, the ply files of the
3d models according to the baseline estimation and a spike to volume mapping file.

```
gen-base --input data/lab_images_no_bar --ply-output data/lab_image_plys --mask-output data/lab_images_spike_mask --volumes-output data/volume_baseline.csv
```

For this to work it is important to download pretained weights for segmentation and imputing in the following locations:

Segment anything weights: [ViT-H SAM model](https://dl.fbaipublicfiles.com/segment_anything/sam_vit_h_4b8939.pth)
They have to be placed in `data/models/segment_anything/sam_vit_h_4b8939.pth`

#### flat-scans
This script is to convert the nested folder struction, which is the output of the original ply
scans to a flatted files structure. I.e in the input directory we assume to have one folder for
each spike and in this folder a ply file containing the mesh of the spike. This is then
simply transformed to a folder containing only the ply files with the corresponding names.

```
flat-scans --input-dir data/scans_11_12_2023 --output-dir data/scans_11_12_2023_flat
```

#### rm-invalid-vol

This script works (just as all other scripts) in the poetry shell.
It removes all mappings form the given mapping input file with volumes == -1.
Such mapping occured because for some spikes the 3D scans were not good enough to extract the volume.

```
rm-invalid-vol --mapping-file data/vol_mapping.csv
```

#### img_processor

This script works (just as all other scripts) in the poetry shell.
It crops the image to a squared image and then it resizes the image to a certain size.

```
img_processor --path data/images_no_bar/ --output data/images_nobar_proc/ --size 256
```

#### merge-datasets

Merges two datasets from to folders into one big new dataset
Each dataset is a directory with images and a vol_mapping.csv file
```
merge-dataset --dataset-1 data/dataset1 --dataset-2 data/dataset2 --output-dataset data/output_dataset
```

#### main_resnet.py and counting_spikelets_resnet.py

Creates embeddings (if they are not available yet) and used them to trains the neural network. These scripts don't have the option to use arguments. Paths need to be adapted. The script main_resnt.py trains a network to estimate the spike volume, while counting_spikelets_resnet.py trains a model to count the number of spikelets per spike. 

#### predict_volumes.py 

This script is used to predict the volumes of the "unseen" spikes using the trained model. 

```
predict_volumes.py --image_path data/images_no_bar --model_path data/model --output_path data/output
```

#### compare_volumes.py

With this script, statistics for a single dataset or a comparison between two data sets can be calculated. For a single data set, the mean and variance are calculated, and a histogram is plotted. To campare predictions and the true values, different metrics are calculated: mean, std, max and mean error, relative mean, std, max and min error, MAE, MSE, RSME, MAPE, and R2. 

```
compare_volumes.py --volumes data/mapping.csv --estimations data/mapping_val.csv --baseline --scaling --name "true_vs_predicted_baseline" --outputfolder data/output
```

The argument `--baseline` is a flag. This data set is treated differently, since predictions below 3000 were clearly wrong and had to be removed. The argument `--scaling` is a flag and a scaling of the predicted output based on the training set predictions can be applied (e.g. adding the difference in mean between true values and predicted values of the training set to the predictions of the validation set). This was not applied so far in our results, just tested. 
The argument `--name` is used to choose a name for the analysis, in order to have a unique name for each comparison. This name is used to create a name for the images and csv files containing the statistics. 

#### plot_results.py

This file is used to make one large plot from the true values und the predicted values of the chosen best models. 

```
plot_results.py --true_volume data/vol_mapping.csv --estimation_1 data/mapping_val1.csv --volume_1 data/vol_mapping1.csv --estimation_2 data/mapping_val2.csv --volume_2 data/vol_mapping2.csv --estimation_3 data/mapping_val3.csv --volume_3 data/vol_mapping3.csv --estimation_4 data/mapping_val4.csv --volume_4 data/vol_mapping4.csv
```
Output path for images needs to be adapted. 
