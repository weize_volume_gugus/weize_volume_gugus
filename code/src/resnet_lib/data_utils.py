import torch
import numpy as np
from torch.utils.data import DataLoader, TensorDataset, Dataset
from torchvision.transforms import v2
from torchvision.io import read_image
from pathlib import Path
import matplotlib.pyplot as plt
import pandas as pd
from tqdm import tqdm 
from icecream import ic

class SpikeInferenceLoader(Dataset):
    
    def __init__(self,
                 image_dir : Path | str,
                 dtype : torch.dtype = torch.float32,
                 ):
        """Image loader with lazy image loading for machines with
        lower memory.
        
        For image to be recognized, it must exist in the directory
        under the name given in the column 'img_name' of the
        file in volume_mapping_path.
        
        Downsizes images to 256 times 256 as resnet only works
        Images are. Herefore we first do a center crop
        so it is advantageous to place the desired object in the middle
        of the image. After the corping image gets rescaled.
        
        Randomly rotates the image if `rotate` true is given.
        This can be done for exaple in combination with n_duplicates
        to 'upsample' dataset.
        

        Args:
            image_dir (Path | str): image direcotry containing images
            volume_mapping_path (Path | str): csv file with column 'volume'
                and 'img_name' containing the image name e.g. '10_2_1_2.jpg'
            rotate (bool, optional): Flag indicating whether or not to rotate images. Defaults to False.
            n_duplicates (int, optional): Number of duplications of image. Defaults to 2.
            dtype (dtype, optional): dtype of image. Defaults to float32.
        """
        
        self.image_dir = Path(image_dir)
        
        assert image_dir.exists(), "Image directory does not exist."
        
        image_names = sorted([img.name for img in self.image_dir.iterdir()])
        
        # Filter only names with '.jpeg' or '.png' extensions
        image_names = [name for name in image_names if name.lower().endswith(('.jpeg', '.png', '.jpg'))]
        
        # check 
        self.image_mapping = pd.DataFrame.from_dict({
            'img' : image_names,
            })

        self.dtype = dtype
        
        print(f'Found {len(image_names)} images.')


    def __len__(self):
        return self.image_mapping.shape[0]
    
    def __getitem__(self, idx):
        
        name = self.image_mapping.loc[idx, 'img']
        
        # read image and transform to floating point type
        torch_image = read_image(str(self.image_dir / name))
        torch_image = (torch_image.type(self.dtype) / 255).clamp(0,1)
        shape = torch_image.shape # (3, h, w)
        short_side = int(np.min(shape[1:]))
        
        # reisze, normalize and turn
        transform_list =[ 
            v2.CenterCrop(short_side),
            v2.Resize(256, antialias=True),
            v2.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
        ]
        transform = torch.nn.Sequential(*transform_list)
        torch_image = transform(torch_image)
        
        return torch_image, name

class SpikeImageLoader(Dataset):
    
    def __init__(self, image_dir : Path | str,
                 volume_mapping_path : Path | str,
                 rotate : bool = False,
                 n_duplicates : int = 1,
                 dtype : torch.dtype = torch.float32,
                 ):
        """Image loader with lazy image loading for machines with
        lower memory.
        
        For image to be recognized, it must exist in the directory
        under the name given in the column 'img_name' of the
        file in volume_mapping_path.
        
        Downsizes images to 256 times 256 as resnet only works
        Images are. Herefore we first do a center crop
        so it is advantageous to place the desired object in the middle
        of the image. After the corping image gets rescaled.
        
        Randomly rotates the image if `rotate` true is given.
        This can be done for exaple in combination with n_duplicates
        to 'upsample' dataset.
        

        Args:
            image_dir (Path | str): image direcotry containing images
            volume_mapping_path (Path | str): csv file with column 'volume'
                and 'img_name' containing the image name e.g. '10_2_1_2.jpg'
            rotate (bool, optional): Flag indicating whether or not to rotate images. Defaults to False.
            n_duplicates (int, optional): Number of duplications of image. Defaults to 2.
            dtype (dtype, optional): dtype of image. Defaults to float32.
        """
        
        self.image_dir = Path(image_dir)
        self.volume_mapping_path = Path(volume_mapping_path)
        self.n_duplicates = n_duplicates
        self.rotate = rotate
        
        assert image_dir.exists(), "Image directory does not exist."
        assert volume_mapping_path.exists(), "Volume mapping does not exist."
        
        image_names = sorted([img.name for img in self.image_dir.iterdir()])
        df_mapping = pd.read_csv(self.volume_mapping_path)
        
        # check 
        images_with_volume = []
        volumes_with_image = []
        print(f'Checking images in {self.image_dir} for volume in {self.volume_mapping_path}')
        for idx, row in df_mapping.iterrows():
            
            # get image
            image_name_mapping = row['img_name']
            if image_name_mapping in image_names:
                images_with_volume.append(image_name_mapping)
                volumes_with_image.append(row['volume'])
        
        self.image_mapping = pd.DataFrame.from_dict({
            'img' : images_with_volume*n_duplicates,
            'volume' : volumes_with_image*n_duplicates})

        self.dtype = dtype
        
        print(f'Found {len(images_with_volume)} images with volume.')


    def __len__(self):
        return self.image_mapping.shape[0]
    
    def __getitem__(self, idx):
        
        name = self.image_mapping.loc[idx, 'img']
        volume = self.image_mapping.loc[idx, 'volume']
        
        # read image and transform to floating point type
        torch_image = read_image(str(self.image_dir / name))
        torch_image = (torch_image.type(self.dtype) / 255).clamp(0,1)
        shape = torch_image.shape # (3, h, w)
        short_side = int(np.min(shape[1:]))
        
        # reisze, normalize and turn
        transform_list =[ 
            v2.CenterCrop(short_side),
            v2.Resize(256, antialias=True),
            v2.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
            v2.RandomHorizontalFlip(p=0.5),
            v2.RandomVerticalFlip(p=0.5),
        ]
        if self.rotate:
            transform_list.append(v2.RandomRotation((0,360)))
        transform = torch.nn.Sequential(*transform_list)
        torch_image = transform(torch_image)
        
        return torch_image, volume

def get_loader_from_np(X : np.array, y : np.array = None, train : bool = True, batch_size=264, shuffle=True, num_workers = 8):
    """
    Create a torch.utils.data.DataLoader object from numpy arrays containing the data.

    input: X: numpy array, the features
           y: numpy array, the labels
    
    output: loader: torch.data.util.DataLoader, the object containing the data
    """
    if train:
        dataset = TensorDataset(torch.from_numpy(X).type(torch.float), 
                                torch.from_numpy(y).type(torch.float))
    else:
        dataset = TensorDataset(torch.from_numpy(X).type(torch.float))
    loader = DataLoader(dataset=dataset,
                        batch_size=batch_size,
                        shuffle=shuffle,
                        pin_memory=True, num_workers=num_workers)
    return loader
