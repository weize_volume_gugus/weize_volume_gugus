from pathlib import Path
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from torchvision.models import ResNet
import resnet_lib.embeddings as embeddings
import resnet_lib.split_dataset as split_dataset
from icecream import ic

def split(
        mapping_file_path : Path, 
        save_mapping_train : Path, 
        save_mapping_val : Path,
        rel_test_set_size : float = 0.2,
        ):
    
    """
    Function to split the data set in training and validation 
    
    Ouput: 
    - stores two mapping files as csv - one for training set, one for validation set

    The validation set will only contain non artificial images which is enforced
    by naming convention, as all the artificial files have a postfix starting
    with b or c, these files are ignored when building the test set.
    
    Note thought that the rel_test_size is relative to all data including
    artificial ones.
    
    Aritificial images for the validation set are only considered if there are no real images at all.
    Otherwise, rel_test_size is merely a upper bound for the test set size.

    Args: 
        mapping_file_path (Path): Path and name  to the mapping file of all images
        save_mapping_trains (Path): Path and name of the new mapping file of training set
        save_mapping_val (Path): Path an name of the new mapping file of validation set 
        rel_test_set_size (float): Relative proportion of data to be used as test set.
    
    """
    
    #read in mapping file
    mapping_file = Path(mapping_file_path)
    df_mapping = pd.read_csv(mapping_file)
    
    # get plants
    plant_id = df_mapping.img_name.str.extract(r'^(\d{1,2}_\d{1,2}_\d{1,2})_\d{1,3}(_\d{1,3})?_(\w)')
    df_mapping['plant_id'] = plant_id.iloc[:,0]

    # filter filter artificial plants
    artificial_grayscale = plant_id.iloc[:,2].str.match('b')
    artificial_color = plant_id.iloc[:,2].str.match('c')
    artificial = np.array(artificial_color,dtype=bool) | np.array(artificial_grayscale,dtype=bool)

    # get real plants and artifical plants
    real_img_plant_id = plant_id.iloc[~artificial,0]
    n_img = artificial.shape[0]
    n_real_img = np.sum(~artificial)
    n_arti_img = np.sum(artificial)

    # get uique ids
    unique_plant_id = plant_id.drop_duplicates()
    unique_real_img_plant_id = real_img_plant_id.drop_duplicates()
    n_unique_plants = unique_plant_id.shape[0]
    n_unique_real_img_plants = unique_real_img_plant_id.shape[0]
    
    # set random seed and split images in mapping file into train set and validation set
    # Splitting the data into training and validation sets
    only_arti = n_real_img == 0
    if only_arti:
        _ , val_plants = train_test_split(unique_plant_id[0], test_size=rel_test_set_size, random_state=42)
    else:
        # split only non_artifical plants and keep idx
        rel_test_set_size = min(rel_test_set_size * n_unique_plants / n_unique_real_img_plants, n_unique_real_img_plants)
        ic(rel_test_set_size)
        _ , val_plants = train_test_split(unique_real_img_plant_id, test_size=rel_test_set_size, random_state=42)
    
    val_idx = np.arange(df_mapping.shape[0])[df_mapping['plant_id'].isin(val_plants)]
    val_mask = np.zeros(df_mapping.shape[0],dtype=bool)
    val_mask[val_idx] = True
    
    train_mask = ~val_mask

    # drop validation on artificial data
    n_lost_arti = int(np.sum(val_mask & artificial & ~only_arti))
    ic(only_arti)
    if not only_arti:
        val_mask = val_mask & ~artificial

    train_df = df_mapping.loc[train_mask]
    val_df = df_mapping.loc[val_mask]

    assert np.intersect1d(train_df.plant_id, val_df.plant_id).shape[0] == 0
    assert n_lost_arti + train_df.shape[0] + val_df.shape[0] == df_mapping.shape[0]
    
    print(f"Images in mapping: {n_img}.")
    print(f"Artificial images thereof: {n_arti_img}.")
    print(f"Real images thereof: {n_real_img}.")
    print(f"Total train set size {train_df.shape[0]}")
    print(f"Total validation set size {val_df.shape[0]}")
    print(f"Real images in train set {np.sum(train_mask & ~artificial)}")
    print(f"Artificial images in train set {np.sum(train_mask & artificial)}")
    print(f"Artificial images removed because in vlidation set {n_lost_arti}.")

    train_df.to_csv(save_mapping_train, index=False)
    val_df.to_csv(save_mapping_val, index=False)

    
def split_embeddings(
        mapping_file_path : Path, 
        mapping_train_path : Path, 
        mapping_val_path : Path, 
        embeddings_path_train : Path, 
        embeddings_path_val : Path, 
        image_folder_path : Path, 
        embedding_neural_net : ResNet
        ):
    
    """
    This function creates embeddings for the validation set and the training set.

    Returns:
        X_all_test (npt.ArrayLike): embeddings of training set
        y_all_test (npt.ArrayLike): labels of training set
        X_all_val (npt.ArrayLike): embeddings of validation set
        y_all_val (npt.ArrayLike): labels of validation set 
    
    Args: 
        mapping_file_path (Path): path and name to mapping file with all available spikes, 
        mapping_train_path (Path): path and name to mapping file of training set, 
        mapping_val_path (Path): path and name to mapping file of validation set, 
        embeddings_path_train (Path): path and name of embedding file for training set, 
        embeddings_path_val (Path): path and name of embedding file for validation set, 
        image_folder_path (Path): path to image folder, 
        embedding_neural_netResNet): chosen model and initialized pretrained weights
     
    """
    
    
    split_dataset.split(mapping_file_path=mapping_file_path, 
                        save_mapping_train=mapping_train_path, 
                        save_mapping_val=mapping_val_path)
    

    #Check if the embeddings already exist
    if not embeddings_path_train.is_file() or not embeddings_path_val.is_file():
        # create directory if it does not exists yet
        # parents = True creates all directories
        # exist = True just makes sure the path exists, every folder 
        #   that already exists is left untouched
        embeddings_path_train.parents[0].mkdir(parents=True, exist_ok=True)
        embeddings_path_val.parents[0].mkdir(parents=True, exist_ok=True)
        # Create genereate train embeddings
        embeddings.generate_embeddings(model=embedding_neural_net,
                                       embeddings_path=embeddings_path_train,
                                       image_folder=image_folder_path,
                                       mapping_file=mapping_train_path)
        
        # Create genereate validation embeddings
        embeddings.generate_embeddings(model=embedding_neural_net,
                                       embeddings_path=embeddings_path_val,
                                       image_folder=image_folder_path,
                                       mapping_file=mapping_val_path)
    
    
    # load embeddings
    X_all_test, y_all_test = embeddings.get_embedings(embeddings_path=embeddings_path_train)
    X_all_val, y_all_val = embeddings.get_embedings(embeddings_path=embeddings_path_val)
    
    return X_all_test, y_all_test, X_all_val, y_all_val 
