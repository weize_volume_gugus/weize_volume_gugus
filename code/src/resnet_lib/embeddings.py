import torch
import cv2
import numpy as np
import pandas as pd
from tqdm import tqdm
from pathlib import Path
from typing import Tuple
from torch import nn
from torch.utils.data import DataLoader
from .data_utils import SpikeImageLoader

def generate_embeddings(model : nn,
                        image_folder : Path,
                        mapping_file : Path,
                        embeddings_path : Path,
                        n_upsample : int = 1,
                        rotate_images : bool = False,
                        device : torch.device = torch.device('cpu'),
                        ):
    """
    Generates a file containing the embeddings, for the given parameters.

    This is, all images found in the `mapping_file` will be loaded into
    a np array and then processed with a pretrainded model `model`.
    Note that the method does not change the model at all, it just
    uses it as a function to apply on the images.

    Then it stores the generated embeddings (= features i.e. outputs of the model)
    in the file specified in `embeddings_path`. Each embedding will be one row of the
    stored matrix, where the last column contains the label (i.e. the volume).

    This means: if the model ouputs vectors of size n, and there are m images found in
    the mapping file and the image folder, then the generated matrix (as a np array,
    stored in `embeddings_path`) will be of dimension (m x (n+1)), i.e. m rows and
    n + 1 colums. From the columns, the first n are the outputs of the pretrained network
    and the last column contains the label (i.e. volume).

    Args:
        model (nn): pretrained model.
        image_folder (Path): Folder containing images.
        mapping_file (Path): File containig the mappings from image to volume MUST INCLUDE COMUMNS: `volume`, `plant_id`, `img_name`.
        embeddings_path (Path): File containing embeddings.
        device (torch.device, optional): device as usual in pytorch. Defaults to torch.device('cpu').
    """
    # make elements to path if they are not already
    if isinstance(image_folder, str):
        image_folder = Path(image_folder)
    if isinstance(mapping_file, str):
        mapping_file = Path(mapping_file)

    # Define a data loader for the dataset
    spike_data = SpikeImageLoader(image_folder, mapping_file, rotate=rotate_images, n_duplicates=n_upsample)
    data_loader = DataLoader(dataset=spike_data,
                             batch_size=256,
                             shuffle=False,
                             pin_memory=True)

    # switch to eval (because of dropout...)
    model.eval()
    model = model.to(device)

    # Remove the last layer to access the embeddings
    model.fc = nn.Identity()

    # Extract the embeddings
    embeddings = []
    print(f'Creating embeddings for images {image_folder} and mapping in {mapping_file.name}:')
    for i, (image_batch, volume_batch)  in tqdm(enumerate(data_loader)):

        # gradient computations in evaluation are pointless so just dont.
        with torch.no_grad():

            # create feature by applying model
            image_batch_on_device = image_batch.to(device)
            feature_batch_on_device = model(image_batch_on_device)

            # move data to cpu and numpy
            feature_batch_np = feature_batch_on_device.cpu().numpy()
            volume_batch_np = volume_batch.numpy()

            # make column vector out of volume_batch
            volume_batch_np = volume_batch_np.reshape((-1,1))

            # add last column containing the labells
            features_labelled = np.concatenate((feature_batch_np, volume_batch_np), axis=1)
            embeddings.append(features_labelled)

    # the list embeddings will contain np tensors of size (n_batch, n_features)
    # then we need to get a vector of size (n_images, n_features)
    # so np.concatenate(..., axis=0) glues the elements in the list together along the first axis
    embeddings = np.concatenate(embeddings, axis=0)

    # Save the embeddings to a file
    np.save(embeddings_path, embeddings)


def get_embedings(embeddings_path : Path) -> Tuple[np.array, np.array]:
    """Loads embeddings given in `embeddings_path` into two np arrays
    the first containing the embeddings and the second containing the labels.

    The method assumes the embeddings exist at the given place and hence will
    crash if they are not previously computed

    Args:
        embeddings_path (Path): Path at which the embeddings should be stored.

    Returns:
        Tuple[np.array, np.array]: embeddings array with features, volume array with labels
    """
    if isinstance(embeddings_path, str):
        embeddings_path = Path(embeddings_path)

    if not embeddings_path.is_file():
        raise Exception(f'File {embeddings_path.absolute()} does not exists.')

    # Load the embeddings
    embeddings_label = np.load(embeddings_path)

    # last column is the volumes label
    volumes = embeddings_label[:, -1]

    # the rest of the colums contains the features / embeddings
    embeddings = np.delete(embeddings_label, -1, axis=1)

    # Convert the lists to NumPy arrays
    return embeddings, volumes
