
from torch.utils.data import DataLoader
from typing import Tuple, List
import torch
import numpy as np
import numpy.typing as npt
from tqdm import tqdm
from icecream import ic
from sklearn.model_selection import train_test_split
import pandas as pd

def train_model(data_train: npt.ArrayLike,
                labels_train: npt.ArrayLike,
                data_val : npt.ArrayLike,
                labels_val : npt.ArrayLike, 
                model: torch.nn.Module,
                optimizer: torch.optim.Optimizer,
                loss_function: torch.nn.Module,
                scheduler: torch.optim.lr_scheduler,
                num_epochs: int,
                train_size: float = 0.8,
                num_workers: int = 8,
                #batch_size: int = 256,
                batch_size: int = 32,
                device: torch.device = torch.device('cpu')) -> Tuple[torch.nn.Module, List[float], List[float]]:
    """Trains model on given data.
    Args:
        data (array_like): Input data.
        labels (array_like): Labels.
        model (torch.nn.Module): Neural network to be trained.
        optimizer (torch.optim.Opitmizer): optimizer used for weight updating.
        loss_funciton (torch.nn.Module): loss function to be minimized.
        scheduler (torch.optim.lr_scheduler): mechanisim to updated the learning rate over the epochs.
        num_epochs (int): Number of epochs to be trained
        train_size (float, optional): Proportion of the data to be used for training. Defaults to 0.8.
        num_workers (int, optional): Number of threads to be used. Defaults to 8.
        batch_size (int, optional): Batch size for training. Defaults to 256.
        device (torch.device, optional): Device on which to do computations. Defaults to torch.device('cpu')

    Returns:
        Tuple[torch.nn.Module, List[float], List[float]] : the trained model, training loss, and validation loss
    """

    # Split data into training and validation sets
    #train_data, val_data, train_labels, val_labels = train_test_split(
    #    data, labels, train_size=train_size, random_state=42)

    # set model to training model
    model.train()
    model.to(device)

    # get data loaders
    train_loader = DataLoader(dataset=list(zip(data_train, labels_train)),
                              batch_size=batch_size, shuffle=True, num_workers=num_workers)
    val_loader = DataLoader(dataset=list(zip(data_val, labels_val)),
                            batch_size=batch_size, shuffle=False, num_workers=num_workers)

    ic(len(train_loader), len(val_loader))
    ic(val_loader)
    
    # store losses for logging
    train_loss_log = []
    val_loss_log = []
    largest_loss = []

    # training loop (tqdm is just for nice output but deactivated)
    for epoch in tqdm(range(num_epochs), disable=True):
        # Training
        model.train()
        epoch_train_loss = 0.0
        for i, (X, y) in enumerate(train_loader):
            X, y = X.to(device), y.to(device)

            optimizer.zero_grad()
            outputs = model(X).squeeze()

            # Convert the target values (y) to Float
            y = y.to(torch.float).squeeze()

            # compute loss
            loss = loss_function(outputs, y)

            # update weights
            loss.backward()
            optimizer.step()

            if not scheduler is None:
                scheduler.step()

            epoch_train_loss += loss.item()


        if epoch % 10 == 0:
            learning_rate = optimizer.param_groups[0]['lr']
            ic(learning_rate)

        ic(epoch, "Train Loss:", epoch_train_loss / len(train_loader))
        train_loss_log.append(epoch_train_loss / len(train_loader))

        # Validation
        model.eval()
        epoch_val_loss = 0.0
        with torch.no_grad():
            for i, (X_val, y_val) in enumerate(val_loader):
                X_val, y_val = X_val.to(device), y_val.to(device)
                outputs_val = model(X_val).squeeze()
                y_val = y_val.to(torch.float).squeeze()
                loss_val = loss_function(outputs_val, y_val)
                epoch_val_loss += loss_val.item()
                largest_loss.append(outputs_val - y_val)
               
                #if epoch == len(num_epochs):
                #    diff = outputs_val - y_val
                #    flattened_tensor = diff.view(-1)

            

                #check images with largest loss
                #largest_loss.append = loss_val

               

        print(epoch, "Validation Loss:", epoch_val_loss / len(val_loader))
        val_loss_log.append(epoch_val_loss / len(val_loader))

    return model, train_loss_log, val_loss_log

