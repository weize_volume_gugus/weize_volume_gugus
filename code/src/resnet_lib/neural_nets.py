import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init

class LinearNeuralNet(nn.Module):
    """
    The model class, which defines our classifier.
    """
    def __init__(self):
        """
        The constructor of the model.
        """
        super(LinearNeuralNet, self).__init__()
        self.l1 = nn.Linear(512, 1) 

    def forward(self, x : torch.tensor) -> torch.tensor:
        """Forwards pass of the nerual network

        Args:
            x (torch.tensor): input vector

        Returns:
            torch.tensor: model evaluation
        """
        x = self.l1(x)
        return x


class SimpleNonLinear(nn.Module):
    """
    The model class, which defines our classifier.
    """
    def __init__(self):
        """
        The constructor of the model.
        """
        super(SimpleNonLinear, self).__init__()
        self.l1 = nn.Linear(512, 256) 
        self.l2 = nn.Linear(256, 1) 

    def forward(self, x : torch.tensor) -> torch.tensor:
        """Forwards pass of the nerual network

        Args:
            x (torch.tensor): input vector

        Returns:
            torch.tensor: model evaluation
        """
        non_linear = nn.ELU()
        x = self.l1(x)
        x = non_linear(x)
        x = self.l2(x)
        return x


class AHNN(nn.Module):
    """
    New model with advanced architecture.
    """

    def __init__(self,  num_hidden_layers, activation, input_size=512, hidden_div=2, output_size=1, normalize=False, dropout_rate=0.1):
        """
        The constructor of the model.

        args:
        input_size(int): number of input nodes
        hidden_div(int): divisor which divides the input_size
        output_size(int): number of output nodes
        num_hidden_layers(int): number of hidden layers
        dropout_rate(float, default=0.5): Rate of the dropouts
        """
        super(AHNN, self).__init__()

        self.normalize = normalize

        self.hidden_sizes = [int(input_size / (hidden_div ** i)) for i in range(1, num_hidden_layers + 1)]

        self.layers = nn.ModuleList()
        self.batch_norms = nn.ModuleList()
        self.activations = nn.ModuleList()

        for i in range(num_hidden_layers):
            input_size = self.hidden_sizes[i - 1] if i > 0 else input_size
            layer = nn.Linear(input_size, self.hidden_sizes[i])
            batch_norm = nn.BatchNorm1d(self.hidden_sizes[i])

            setattr(self, f"layer{i + 1}", layer)
            setattr(self, f"batch_norm{i + 1}", batch_norm)
            setattr(self, f"activ{i + 1}", activation)

            self.layers.append(layer)
            self.batch_norms.append(batch_norm)
            self.activations.append(activation)

        self.output_layer = nn.Linear(self.hidden_sizes[-1], output_size)
        self.dropout = nn.Dropout(dropout_rate)

        # Initialize linear layers
        for layer in [*self.layers, self.output_layer]:
            init.xavier_uniform_(layer.weight, gain=init.calculate_gain('relu'))

    def forward(self, x):
        # Convert input tensor to float32 if it's not already
        x = x.float() if x.dtype != torch.float32 else x

        for layer, batch_norm, activation in zip(self.layers, self.batch_norms, self.activations):
            if self.normalize:
                x = self.dropout(activation(batch_norm(layer(x))))
            else:
                x = self.dropout(activation(layer(x)))

        x = self.output_layer(x)
        return x

    def get_data(self):
        architecture = [] 
        return self.hidden_sizes


