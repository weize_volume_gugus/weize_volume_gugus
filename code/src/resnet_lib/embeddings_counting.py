import torch
import cv2
import numpy as np
import pandas as pd
from tqdm import tqdm
from pathlib import Path
from typing import Tuple
from torch import nn
from torch.utils.data import DataLoader

def get_image_iden_array(image : Path,  out_shape : Tuple[int, int] = (300,300)) ->  np.array:
    """Load a single image at a given path to a numpy array
    
    The input image can have any size, but the image is scaled within
    the method to the chosen height, which defaults to (300, 300).
    Note that the channles will remain unchanged and by default, 
    the last axis are the channles in numpy (as opposed to pytorch, where the channles
    are in the first axis).
    
    Args:
        image (Path): Path to the image
        out_shape (Tuple[int, int], optional): width and height of the output image (array). Defaults to (300, 300)

    Raises:
        Exception: If given path is invalid

    Returns:
        np.array: numpy array with image resized to `out_shape`
    """
    if not image.is_file():
        raise Exception(f'Image path {image.absolute()} is invalid.')

    # read image file
    cv_img = cv2.imread(str(image))
    cv_img_small = cv2.resize(cv_img, out_shape)
    
    # convert to numpy
    np_img = np.asarray(cv_img_small, dtype=np.float32)
    
    return np_img

def get_images_from_path(image_folder : Path, mapping_file : Path) -> Tuple[torch.tensor, torch.tensor, pd.Series]:
    """Generates a tuple containing (Number labels as torch tensor, images stacked in torch tensor, plant_ids as pd.Series)
    
    The method reads the file mapping_file and for each entry found, it looks for the image with the name in 
    the column `img_name`, then it stores the image and the number with the same index in the 
    corresponding tensors, which are returned separately in the tuple.

    Args:
        image_folder (Path): Folder containing images.
        mapping_file (Path): File containig the mappings from image to number MUST INCLUDE COMUMNS: `total`, `plant_id`, `img_name`.

    Raises:
        Exception: For missing folder, missing mapping, and missing files (in the called subfunction)

    Returns:
        Tuple[torch.tensor, torch.tensor, pd.Series]: Generates a tuple containing 
            (number labels as torch tensor, images stacked in torch tensor, plant_ids as pd.Series)
    """

    if not image_folder.is_dir():
        raise Exception(f'The given folder {image_folder.absolute()} does not exist.')
    
    if not mapping_file.is_file():
        raise Exception(f'Given mapping {mapping_file.absolute()} does not exist.')
    
    # read mapping info
    df_mapping = pd.read_csv(mapping_file)
    
    totals = []
    plant_ids = []
    images = []
    
    print(f'Getting images from {image_folder}:')
    for idx, row in tqdm(df_mapping.iterrows()):
        
        # get image
        image_name = row['img_name']
        image_path = Path(image_folder,image_name)
        image = get_image_iden_array(image_path)
        
        # append to lists
        images.append(image)
        totals.append(row['total'])
        plant_ids.append(row['plant_id'])
        
    # make one big array
    np_images = np.stack(images, axis=0)
    
    # numpy / opencv image shape: [batch, width, height, channels]
    # pytorch image shape: [batch, channels, width, height]
    np_images = np_images.swapaxes(1,3)
    torch_images = torch.from_numpy(np_images)
    
    # create labels as torch tensor
    np_totals = np.stack(totals, dtype=np.float32)
    torch_totals = torch.from_numpy(np_totals)
    
    # create plant id series
    df_plant_ids = pd.Series(plant_ids)

    return (torch_totals, torch_images, df_plant_ids)

def generate_embeddings(model : nn,
                        image_folder : Path,
                        mapping_file : Path,
                        embeddings_path : Path,
                        device : torch.device = torch.device('cpu'),
                        ):
    """
    Generates a file containing the embeddings, for the given parameters.

    This is, all images found in the `mapping_file` will be loaded into
    a np array and then processed with a pretrainded model `model`.
    Note that the method does not change the model at all, it just
    uses it as a function to apply on the images. 
    
    Then it stores the generated embeddings (= features i.e. outputs of the model)
    in the file specified in `embeddings_path`. Each embedding will be one row of the
    stored matrix, where the last column contains the label (i.e. the number).
    
    This means: if the model ouputs vectors of size n, and there are m images found in
    the mapping file and the image folder, then the generated matrix (as a np array, 
    stored in `embeddings_path`) will be of dimension (m x (n+1)), i.e. m rows and 
    n + 1 colums. From the columns, the first n are the outputs of the pretrained network
    and the last column contains the label (i.e. number).

    Args:
        model (nn): pretrained model.
        image_folder (Path): Folder containing images.
        mapping_file (Path): File containig the mappings from image to number MUST INCLUDE COMUMNS: `total`, `plant_id`, `img_name`.
        embeddings_path (Path): File containing embeddings.
        device (torch.device, optional): device as usual in pytorch. Defaults to torch.device('cpu').
    """
    # make elements to path if they are not already
    if isinstance(image_folder, str):
        image_folder = Path(image_folder)
    if isinstance(mapping_file, str):
        mapping_file = Path(mapping_file)

    # Define a data loader for the dataset
    totals, images, plant_ids = get_images_from_path(image_folder, mapping_file)
    
    # define dataloader note that this is preprocessing hence batch size is not too important
    data_loader = DataLoader(list(zip(images, totals)), batch_size=64, pin_memory=True)

    # switch to eval (because of dropout...)
    model.eval()
    model = model.to(device)

    # Remove the last layer to access the embeddings
    model.fc = nn.Identity()

    # Extract the embeddings
    embeddings = []
    print(f'Creating embeddings for images {image_folder} and mapping in {mapping_file.name}:')
    for i, (image_batch, total_batch)  in tqdm(enumerate(data_loader)):

        # gradient computations in evaluation are pointless so just dont.
        with torch.no_grad():

            # create feature by applying model
            image_batch_on_device = image_batch.to(device)
            feature_batch_on_device = model(image_batch_on_device)

            # move data to cpu and numpy
            feature_batch_np = feature_batch_on_device.cpu().numpy()
            total_batch_np = total_batch.numpy()
            
            # make column vector out of total_batch
            total_batch_np = total_batch_np.reshape((-1,1))

            # add last column containing the labells
            features_labelled = np.concatenate((feature_batch_np, total_batch_np), axis=1)
            embeddings.append(features_labelled)

    # the list embeddings will contain np tensors of size (n_batch, n_features)
    # then we need to get a vector of size (n_images, n_features)
    # so np.concatenate(..., axis=0) glues the elements in the list together along the first axis
    embeddings = np.concatenate(embeddings, axis=0)
    
    # Save the embeddings to a file
    np.save(embeddings_path, embeddings)
    

def get_embedings(embeddings_path : Path) -> Tuple[np.array, np.array]:
    """Loads embeddings given in `embeddings_path` into two np arrays
    the first containing the embeddings and the second containing the labels.

    The method assumes the embeddings exist at the given place and hence will
    crash if they are not previously computed

    Args:
        embeddings_path (Path): Path at which the embeddings should be stored.

    Returns:
        Tuple[np.array, np.array]: embeddings array with features, number array with labels
    """
    if isinstance(embeddings_path, str):
        embeddings_path = Path(embeddings_path)
    
    if not embeddings_path.is_file():
        raise Exception(f'File {embeddings_path.absolute()} does not exists.')

    # Load the embeddings
    embeddings_label = np.load(embeddings_path)
    
    # last column is the numbers label
    totals = embeddings_label[:, -1]
    
    # the rest of the colums contains the features / embeddings
    embeddings = np.delete(embeddings_label, -1, axis=1)
    
    # Convert the lists to NumPy arrays
    return embeddings, totals
