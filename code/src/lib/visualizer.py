import matplotlib.pyplot as plt

class Visualizer:
    def __init__(self):
        """
        Class to visualize the training and validation data
        """
        self.fig, self.ax = plt.subplots()

    def plot_loss(self, train_loss, val_loss, num_epochs, save_path = None):
        """
        Visualize the data
        Args:
        train_loss(Array): training loss of every epoch
        val_loss(Array): validation loss of every epoch
        num_epochs(int): Number of epochs
        """
        epochs = list(range(1, num_epochs + 1))

        # Plot training loss in blue
        self.ax.plot(epochs, train_loss, label='Train Loss', color='blue')

        # Plot validation loss in red
        self.ax.plot(epochs, val_loss, label='Validation Loss', color='red')

        # Add labels and title
        self.ax.set_xlabel('Epoch')
        self.ax.set_ylabel('Loss')
        self.ax.set_title('Training and Validation Loss')

        # Add legend
        self.ax.legend()

        if not save_path is None:
            self.fig.savefig(save_path)
        else:
            # Show the plot
            plt.show()


