from icecream import ic
import numpy as np
from typing import Optional, Tuple, List
import cv2
import numpy.typing as npt
from copy import deepcopy
import matplotlib.pyplot as plt
from skimage import morphology
from scipy import interpolate
from pathlib import Path
from lib.img_segmenter import ImageSegmenter, print_time
from lib.abstract_spike import AbstractSpike

class SpikeSegmenter(ImageSegmenter):

    def __init__(self, segmentation_model : Tuple[str, str], verbose : bool = False):
        """Class to Segment spike and create an abstract representation of spike

        Args:
            segmentation_model (Tuple[str,Path]): Tuple with segment_anything type and model checkpoint
            verbose (bool, optional): Print times for each step. Defaults to False.
        """
        
        # set function timer features
        ImageSegmenter.__init__(self, segmentation_model=segmentation_model, verbose=verbose) 
    
    def get_points(self, image : npt.ArrayLike, num_points = 1) -> npt.NDArray:
        """Compute points on spike, which have the right hue, value and s

        Image is assumed to be blurred and resized in the input already

        Args:
            image (npt.ArrayLike): Image containig bars on the left side centered

        Returns:
            npt.NDArray: two coordinates within the image
        """
        
        # convert to correct color scheme
        hsv_image = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
        
        # hue in the range of spikes
        spike_mask = (hsv_image[:,:,0] <= 25) & (hsv_image[:,:,0] >= 10)
        
        # select point with max value
        max_hue_val_sat = np.flip(np.sort(hsv_image[spike_mask], axis=0),axis=0)[num_points]
        spike_mask = spike_mask & (hsv_image[:,:,1] >= max_hue_val_sat[1])

        # select point with max saturation
        max_hue_val_sat = np.flip(np.sort(hsv_image[spike_mask], axis=0),axis=0)[num_points]
        spike_mask = spike_mask & (hsv_image[:,:,2] >= max_hue_val_sat[2])

        # get point coordinates
        spike_points = np.flip(np.argwhere(spike_mask),axis=1)
        
        if num_points == 1:
            spike_points = spike_points[[0]]
        else:
            spike_points = spike_points[:num_points]
        
        return spike_points

    # time is printed within segmentor
    def segment_spike(self, rgb_image : npt.NDArray) -> npt.NDArray:
        """Computes spike from rgb image

        Args:
            rgb_image (npt.NDArray): image segmentation

        Returns:
            npt.NDArray: mask for the segmented spike
        """
        select_index = 1
        kernel_size = 13
        num_points = 1
        downsize = 2024 # note that kernel size is absolute, so samller downsize => larger relative kernel
        mask = self.segment_pipline(rgb_image=rgb_image,select_index=select_index, kernel_size=kernel_size, resize=downsize, num_points=num_points)
        
        # remove mask artefacts
        mask = mask.astype(np.uint8)
        element = cv2.getStructuringElement(cv2.MORPH_RECT, (5,5), (2,2))
        mask = cv2.erode(mask,element,iterations=8)
        mask = cv2.dilate(mask,element,iterations=8)

        return mask
    
    def next_neighbours(self, padded_skeleton : npt.NDArray, point : npt.NDArray, max_d : int) ->  Optional[npt.NDArray]:
        """Compute neighbours of given point if there exists one
        
        NOTE: If the skeleton in padded_skeleton goes to up until the border of the image, algorithm might thow
        and out of bound error. Idea to fix this, is to add 0 padding. BUT this is the CALLERS BURDON for
        computational efficiency reasons.
        
        Args:
            padded_skeleton (npt.NDArray): skeleton with 0 on the outermost pixels of the array
            point (npt.NDArray): current point
            prev_neighbours (npt.NDArray): already visited points
            max_d (int): maximal distance to look for neighbours before returning None
        
        Returns:
            Optional[npt.NDArray]: List of neighbour points
        """
        x = point[0]
        y = point[1]

        # delta x, delta y
        d = 1
        neighbours = padded_skeleton[x-d:x+1+d, y-d:y+1+d]
        while np.all(~neighbours) and d < max_d:
            # allow for larger hops
            d = d + 1
            neighbours = padded_skeleton[x-d:x+1+d, y-d:y+1+d]
            
        num_neighbours = np.sum(neighbours)
        
        if num_neighbours == 0:
            # no next neighbour
            return None

        next_points_local = np.argwhere(neighbours.astype(bool))
        
        # compute global location
        next_point = point + next_points_local - (d,d)
        
        return next_point

    def unset_skeleton(self, skeleton : npt.NDArray, points : npt.NDArray, distance : int = 0) -> None:
        """Set points to zero on skeleton

        Args:
            skeleton (npt.NDArray): sekeleton to be updated
            points (npt.NDArray): points to set to zero
        """
        d = distance
        
        if points.ndim == 1:
            x, y = points
            skeleton[x-d : x+1+d, y-d : y+1+d] = False
        elif points.ndim == 2:
            for x,y in points:
                skeleton[x-d : x+1+d, y-d : y+1+d] = False

            
    def get_skeleton_path(self, padded_skeleton : npt.NDArray, first_point : npt.NDArray, max_d : int, initial_point : bool = False) -> List[npt.NDArray]:
        """Compute the longest possible path starting at the given point
        
        function recurses if there are mutliple options
        
        If inital points flat is given, 2 paths might be returned

        Args:
            padded_skeleton (npt.NDArray): padded skeleton in which to search for a longest path
            first_point (npt.NDArray): starting points to search for path
            max_d (int): maximal distance
            initial_point (bool): indicator if the point is initial point, might return two concated path if it is

        Returns:
            List[npt.NDArray]: list of points in order which give a path
        """

        self.unset_skeleton(skeleton=padded_skeleton, points=first_point)
        path = [first_point]
        
        # get next points
        next_points = self.next_neighbours(padded_skeleton=padded_skeleton, point=first_point, max_d=max_d)
        
        # loop to check for new paths
        while not next_points is None:

            if next_points.shape[0] > 1:
                # recurse to get longest path
                self.unset_skeleton(skeleton=padded_skeleton, points=next_points, distance=max_d-1)
                subpaths = []
                for point in next_points:
                    subpaths.append(self.get_skeleton_path(padded_skeleton=padded_skeleton, first_point=point, max_d=max_d))
                
                # sort descending
                subpaths = sorted(subpaths, key=lambda path : -len(path))
                if initial_point:
                    second_subpath = subpaths[1]
                    second_subpath.pop(0)
                    second_subpath = list(reversed(second_subpath))
                    path = second_subpath + subpaths[0]
                else:
                    path = path + subpaths[0]
                return path
                
            else:
                # only one single neighbour found
                initial_point = False
                next_point = next_points[0]
                path.append(next_point)
                self.unset_skeleton(skeleton=padded_skeleton, points=next_point)
                next_points = self.next_neighbours(padded_skeleton=padded_skeleton, point=next_point, max_d=max_d)
        
        return path

    def save_axis(self,image : npt.NDArray, dir : Path, img_id : str):
        """If the image was segmented beforehand and abstract spike was
        created, then this function saves the main_curve, main_axis and
        skeleton into the given folder.
        
        This function is mainly for debugging, documentation and visualization
        it does not have any functional purpose apart from this.

        Args:
            image (npt.NDArray): image to plot onto
            dir (Path): directory where to put the files
            img_id (str): File names
        """
        dir.mkdir(parents=True, exist_ok=True)
        image = deepcopy(image)
        dpi = 96
        img_shape = tuple(i for i in np.flip(image.shape[:2]) // dpi)
        color = "red"
        linewidth = 10
        if hasattr(self, "main_curve") and not self.main_curve is None:
            print(f"Saving spline image of {img_id}")
            plt.clf()
            plt.figure(figsize=img_shape, frameon=False)
            plt.axis("off")
            plt.imshow(image)
            plt.plot(self.main_curve[:,1], self.main_curve[:,0], color=color, linewidth=linewidth)
            plt.tight_layout(pad=0)
            plt.savefig(dir / f'{img_id}_curve.jpg', format='jpg', dpi=dpi)
        if hasattr(self, "main_axis") and not self.main_axis is None:
            print(f"Saving main axis of {img_id}")
            plt.clf()
            plt.figure(figsize=img_shape, frameon=False)
            plt.axis("off")
            plt.imshow(image)
            plt.plot(self.main_axis[:,1], self.main_axis[:,0], color=color, linewidth=linewidth)
            plt.tight_layout(pad=0)
            plt.savefig(dir / f'{img_id}_axis.jpg', format='jpg',dpi=dpi)
        if hasattr(self, "width_pairs") and not self.width_pairs is None and hasattr(self, "main_curve") and not self.main_curve is None:
            print(f"Saving width paris of {img_id}")
            plt.clf()
            plt.figure(figsize=img_shape, frameon=False)
            plt.axis("off")
            plt.imshow(image)
            for pair in self.width_pairs:
                plt.plot(pair[:,1], pair[:,0], color=color, linewidth=linewidth)
            plt.plot(self.main_curve[:,1], self.main_curve[:,0], color=color, linewidth=linewidth)
            plt.tight_layout(pad=0)
            plt.savefig(dir / f'{img_id}_widths.jpg', format='jpg',dpi=dpi)

        if hasattr(self, "base_skeleton") and not self.base_skeleton is None:
            print(f"Saving skeleton of {img_id}")
            image[self.base_skeleton.astype(bool)] = [255,0,0]
            plt.clf()
            plt.imsave(dir / f'{img_id}_skeleton.jpg', image)

    @print_time("initialize abstract spline")
    def initialize_abstract_spline(self, np_mask_contour : npt.NDArray) -> AbstractSpike:
        """Generate abstract spline, by creating the skeleton of the mask

        Args:
            np_mask_contour (npt.NDArray): mask of spline contours in the image

        Returns:
            AbstractSpike: abstract representation of the spike
        """
        
        # store original mask shape
        shape = np.array(np_mask_contour.shape)

        # make image smaller
        # note that for larger downsize values, it is adisable to use a larger max_d otherwise
        # jumps small jumps might not be recognized as connected component
        max_d = 5
        downsize = 500
        np_mask = cv2.resize(np_mask_contour.astype(np.uint8), (downsize,downsize))
        
        # compute skeleton
        skeleton = morphology.skeletonize(np_mask.astype(bool))
        self.base_skeleton = cv2.resize(skeleton.astype(np.uint8) * 255, np.flip(shape))
        
        # pad skeleton to make sure get_skeleton_path() works as intended
        padded_skeleton = np.pad(array=skeleton, pad_width=max_d, mode='constant')
        
        # pick any point and start building
        points = np.argwhere(padded_skeleton)
        first_point = points[0]
        
        ###########################################
        # Get order on the points
        ###########################################

        # start one direction
        points = self.get_skeleton_path(padded_skeleton=padded_skeleton, first_point=first_point, max_d=max_d, initial_point=True)
        points = np.array(points)

        ########################################################
        # transform the points back to the original coordinates
        ########################################################
        points = points - (max_d, max_d) # account for padding
        points[:,0] = points[:,0] * shape[0] // downsize + shape[0] // (downsize)  # account for rounding
        points[:,1] = points[:,1] * shape[1] // downsize + shape[1] // (downsize) # account for rounding

        # store for possible plottting
        self.main_axis = deepcopy(points)
        
        # get x and y spline
        lam = 0.00001 # smooting parameter
        time = np.linspace(0,1, points.shape[0])
        x_spline = interpolate.make_smoothing_spline(time, points[:,0], lam = lam)
        y_spline = interpolate.make_smoothing_spline(time, points[:,1], lam = lam)

        # create spike
        spike = AbstractSpike(x_spline=x_spline, y_spline=y_spline, contour_mask=np_mask_contour, verbose=self.verbose)
        
        # compute orthogonal vectors for visualization
        self.width_pairs = []
        for t in np.linspace(0,1,20):
            _, (left,_, right) = spike.radius(t)
            self.width_pairs.append(np.array([left, right]))
            
        
        # store for possible plotting
        self.main_curve = np.array([spike.spline(t) for t in np.linspace(0,1,200)])

        return spike
    
    def segment_and_initialize_spike(self, rgb_image : npt.NDArray) -> AbstractSpike:
        """First extracts spike form image and then initializes abstract spline

        Args:
            rgb_image (npt.NDArray): Image of a single spline

        Returns:
            AbstractSpike: Abstract representation of spline
        """
        
        # compute mask
        contour = self.segment_spike(rgb_image=rgb_image)
        
        # get abstract spike
        spike = self.initialize_abstract_spline(np_mask_contour=contour)
        
        return spike
        



