import torch
import os
import numpy as np
from icecream import ic
import pandas as pd

class Saver:
    def __init__(self, save_dir='saved_models') -> None:
        self.count = 0
        self.save_dir = save_dir
        os.makedirs(self.save_dir, exist_ok=True)

    def save(self, model, train_loss, val_loss, architecture, activation, optimizer, batch_size, dropout_rate, epochs):
        """
        Creates a txt-file where all the informations about the model are saved.
        Saves the model as a model.pth txt-file
        Saves the train_loss array and the val_loss array as npy files 
        """
        self.count += 1

        model_dir = os.path.join(self.save_dir, "models/")
        losses_dir = os.path.join(self.save_dir, "losses/")
        info_dir = os.path.join(self.save_dir, "info/")

        os.makedirs(model_dir, exist_ok=True)
        os.makedirs(losses_dir, exist_ok=True)
        os.makedirs(info_dir, exist_ok=True)
        ic(losses_dir, info_dir, model_dir)

        saved_data = {
            'model_state_dict': model.state_dict(),
            'architecture': architecture,
            'activation': activation,
            'optimizer': optimizer,
            'batch_size': batch_size,
            'dropout_rate': dropout_rate,
            'epochs': epochs,
            'train_loss': train_loss,
            'val_loss': val_loss
        }

        # Save model
        model_filename = f"model{self.count}.pth"
        model_path = os.path.join(model_dir, model_filename)
        ic(model_path)
        torch.save(model.state_dict(), model_path)

        # Save information in a txt file
        info_filename = f"info{self.count}.txt"
        info_path = os.path.join(info_dir, info_filename)

        with open(info_path, 'w') as file:
            file.write(f"Model: {model_filename}\n")
            file.write(f"Architecture: {architecture}\n")
            file.write(f"Activation: {activation}\n")
            file.write(f"Optimizer: {optimizer}\n")
            file.write(f"Batch-Size: {batch_size}\n")
            file.write(f"Dropout Rate: {dropout_rate}\n")
            file.write(f"Epochs: {epochs}\n")

        # Save train_loss and val_loss as npy files
        train_loss_filename = f"train_loss{self.count}.npy"
        val_loss_filename = f"val_loss{self.count}.npy"

        # Creating a dataframe with 
        df_loss = pd.DataFrame({'Trainings_loss': train_loss, 'Validation_loss': val_loss})
        df_loss.to_csv(os.path.join(losses_dir, train_loss_filename), index=False)

        np.save(os.path.join(losses_dir, train_loss_filename), np.array(train_loss))
        np.save(os.path.join(losses_dir, val_loss_filename), np.array(val_loss))

    def save_simple(self, model, train_loss, val_loss, architecture, activation, optimizer, batch_size, dropout_rate, epochs):
        """
        Creates a txt-file where all the informations about the model are saved.
        Saves the model as a model.pth txt-file
        Saves the train_loss array and the val_loss array as npy files 
        """
        self.count += 1

        dir = os.path.join(self.save_dir, "AllInnOne/")
        

        os.makedirs(dir, exist_ok=True)
        

        saved_data = {
            'model_state_dict': model.state_dict(),
            'architecture': architecture,
            'activation': activation,
            'optimizer': optimizer,
            'batch_size': batch_size,
            'dropout_rate': dropout_rate,
            'epochs': epochs,
            'train_loss': train_loss,
            'val_loss': val_loss
        }

        torch.save(saved_data, os.path.join(dir, f"model{self.count}.pth"))

