from pathlib import Path
from typing import Optional
import torch
import pprint
from .seed_setter import SeedSetter
from resnet_lib.neural_nets import AHNN
import numpy as np
from torch import nn
from tqdm import tqdm
from torch.utils.data import DataLoader
from resnet_lib.data_utils import SpikeInferenceLoader
from torchvision import models
import pandas as pd
from icecream import ic

class Inferrer(SeedSetter):

    
    def __init__(self,*,
                    image_file_or_folder_path : Path | str,
                    saved_model_path : Path | str,
                    output_path : Path | str
                 ):
        
        # Fix inferrer seeds (should not make a difference)
        SeedSetter.__init__(self)
        
        self.image_file_or_folder_path = Path(image_file_or_folder_path)
        self.saved_model_path = Path(saved_model_path)
        self.output_path = Path(output_path)
        
        ic(str(self.image_file_or_folder_path))
        if not self.image_file_or_folder_path.exists():
            raise FileNotFoundError(f"Given path does not exist {str(image_file_or_folder_path)}.")
        
        if self.image_file_or_folder_path.is_file():
            self.single = True
        else:
            self.single = False
        
        if torch.cuda.is_available():
            self.device = torch.device('cuda') 
            print("Compute with GPU")
        else:
            self.device = torch.device("cpu")
            print("Compute with CPU")

        config = torch.load(self.saved_model_path)
            
        self.model = AHNN(activation=config['activation'], dropout_rate=config['dropout_rate'], num_hidden_layers=len(config['architecture']))
        self.model.load_state_dict(config['model_state_dict'])
        self.model = self.model.to(self.device)

    def print_saved_model_info(self, path : Path):
        """Prints model with all its items."""
        saved_dict = torch.load(path)
        pprint.pprint(saved_dict,width=80, sort_dicts=True)

    def _load_model(self, path : Path) -> torch.nn.Module:
        """Loads a model from a pickle file assuming
        the file contains a `model_state_dict` entry.

        Args:
            path (Path): path to pth file containing the dictonary
        """
        saved_dict = torch.load(path)
        if not 'model_state_dict' in saved_dict.keys():
            raise KeyError(f"Model not found {path.name}")
        
        model = saved_dict['model_state_dict']
        return model
    
    def infer_pipeline(self):
        """
        Generates embeddings, for the given parameters.

        This is, all images found in the `image_file_or_folder_path` will be loaded into
        a np array and then processed with a pretrainded model `model`.
        Note that the method does not change the model at all, it just
        uses it as a function to apply on the images.

        

        Args:
            model (nn): pretrained model.
            device (torch.device, optional): device as usual in pytorch. Defaults to torch.device('cpu').
        """
        # Define a data loader for the dataset
        spike_data = SpikeInferenceLoader(self.image_file_or_folder_path)
        data_loader = DataLoader(dataset=spike_data,
                                 batch_size=256,
                                 shuffle=False,
                                 pin_memory=True)

        # choose some model and initialize PRETRAINED weights
        embedding_nn = models.resnet18(weights=models.ResNet18_Weights.IMAGENET1K_V1)
         
        # switch to eval (because of dropout...)
        embedding_nn.eval()
        embedding_nn = embedding_nn.to(self.device)

        # Remove the last layer to access the embeddings
        embedding_nn.fc = nn.Identity()

        y_all_std = 1163 # FIXED on empiriccal values
        y_all_mean = 4630 # FIXED on empirical values

        # Extract the embeddings
        embeddings = []
        volumes = []
        img_names = []
        for i, (image_batch, name_batch)  in tqdm(enumerate(data_loader)):

            # gradient computations in evaluation are pointless so just dont.
            with torch.no_grad():

                # create feature by applying model
                image_batch_on_device = image_batch.to(self.device)
                feature_batch_on_device = embedding_nn(image_batch_on_device)
                outputs = self.model(feature_batch_on_device)
                
                outputs = outputs.cpu().numpy().flatten()
                ic(outputs.shape)
                batch_volumes = outputs
                volumes.append(batch_volumes)
                img_names = img_names + name_batch

        volumes = np.concatenate(volumes, axis=0)
        # Creating a DataFrame
        data = {'img_name': img_names, 'volume': volumes}
        df = pd.DataFrame.from_dict(data)

        # Writing the DataFrame to a CSV file
        df.to_csv(self.output_path, index=False)
 

