import os
import open3d as o3d
import cv2
import numpy as np
from PIL import Image
from icecream import ic
from pathlib import Path

class PcdGenerator:
    def __init__(self, rgbPath : str | Path, dptPath : str | Path, outputPath : str | Path) -> None:
        """
        Class to generate pointclouds from given images

        Args:
            inputPath (str | Path): Path to the images which should be transformed to depth image
            outputPath (str | Path): Path to folder where the depth-images should be stored
        """
        self.rgbPath = Path(rgbPath)
        self.dptPath = Path(dptPath)
        self.outputPath = Path(outputPath)
        
        
    def save(self):
        #TODO Fix
        """
        Function which saves the generated depth-images

        Args:
            None
        """
        # visualize the prediction
        output = prediction.squeeze().cpu().numpy()

        formatted = (output * 255 / np.max(output)).astype("uint8")
        depth = Image.fromarray(formatted)

        path = os.path.join(self.outputPath, image_file)

        try:
            with open(path, 'w') as file:
                depth.save(file)
                ic("saved at: ", path)
        except Exception as e:
            ic(e, path)
        
    def generate(self) -> None:
        """
        Function which generates the depth-image predictions
        
        Args:
            None
        """
        
        # Get a list of all image files in the specified path
        image_files = [f for f in self.dptPath.iterdir() if f.suffix in ['.png', '.jpg', '.jpeg']]
        
        for file in image_files:
            
            # straig forward use only depth map
            np_dpt = np.array(Image.open(file),dtype=float)
            height, width = np_dpt.shape
            np_axis_0 = np.repeat(np.arange(width)[np.newaxis,:],height,axis=0).astype(float)
            np_axis_1 = np.repeat(np.arange(height)[:,np.newaxis],width,axis=1).astype(float)
            np_image = np.stack([np_axis_0,np_axis_1,2*np_dpt],axis=2).reshape(-1,3)

            pcd_o3d = o3d.geometry.PointCloud()  # create point cloud object
            pcd_o3d.points = o3d.utility.Vector3dVector(np_image)  # set pcd_np as the point cloud points

            # Visualize:
            o3d.visualization.draw_geometries([pcd_o3d])

            # Aarons intrinsic map approach
            #color_raw = o3d.io.read_image(str(file))
            #dpt_raw = o3d.io.read_image(str(file))
            
            #rgbd_image = o3d.geometry.RGBDImage.create_from_color_and_depth(color_raw, dpt_raw)
            #pcd = o3d.geometry.PointCloud.create_from_rgbd_image(rgbd_image, o3d.camera.PinholeCameraIntrinsic(o3d.camera.PinholeCameraIntrinsicParameters.Kinect2DepthCameraDefault))
            
            #o3d.visualization.draw_geometries([pcd])

if __name__ == "__main__":
    generator = PcdGenerator(Path("data/lab_images_no_bar"), Path("data/dpt_images"), Path("data/pcd_images"))
    generator.generate()

