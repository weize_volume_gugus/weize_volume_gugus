import resnet_lib.training_counting as training_counting
import resnet_lib.embeddings_counting as embeddings
import resnet_lib.neural_nets as neural_nets
import random
import torch.nn as nn
from lib.data_saver import Saver
import torch as torch
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
from torchvision import models
from icecream import ic
from lib.visualizer import Visualizer
import pandas as pd
from sklearn.model_selection import train_test_split
import resnet_lib.split_dataset as split_dataset


if __name__ == "__main__":
    
    # Check if cuda is available to determine device
    if torch.cuda.is_available():
        device = torch.device('cuda')
        print("I have the GPU")
    else:
        device = torch.device('cpu')
        print("Have to use CPU")
    
    # Fix seeds to make all the code reproducible
    torch.manual_seed(0)
    np.random.seed(0)
    random.seed(0)
    
    ### ------------ Set paths ----------------------------------###
    
    dataset_path = Path("/home/zumstego/public/Evaluation/Projects/KP0032_zumstego/Olivia_3D_dataset/3D/images_no_bar")
    output_path = Path("data/counting_no_bar")
    output_path.mkdir(parents=True, exist_ok=True)
    #SET PATHS
    #path to mapping files
    mapping_file_path = "/home/zumstego/public/Evaluation/Projects/KP0032_zumstego/Olivia_3D_dataset/merged_counting_spikelets.csv"
    save_mapping_train = output_path / "mapping_train.csv"
    save_mapping_val = output_path / "mapping_val.csv"

    # choose path to store the embedings after computation
    # must be a `Path` variable!
    embeddings_path_train = output_path / 'resnet18_embeddings_train_100_2.npy'
    embeddings_path_val = output_path / 'resnet18_embeddings_val_100_2.npy'
    
    # path to images (All images which have an entry in the mapping file must be stored in this
    # folder under the name given in the column `img_name` in the mapping)
    image_folder_path = dataset_path
    
    # choose some model and initialize PRETRAINED weights
    embedding_neural_net = models.resnet18(weights=models.ResNet18_Weights.IMAGENET1K_V1) 
        
    X_all_test, y_all_test, X_all_val, y_all_val = split_dataset.split_embeddings(
                mapping_file_path=mapping_file_path, 
                mapping_train_path=save_mapping_train, 
                mapping_val_path=save_mapping_val, 
                embeddings_path_train=embeddings_path_train, 
                embeddings_path_val=embeddings_path_val, 
                image_folder_path=image_folder_path, 
                embedding_neural_net=embedding_neural_net)

    # Saver Class for saving the output data
    saver = Saver(str(output_path)) 
         

    ### ------------ Simple Things to tune START ----------------###
    
    #---BEGIN MODEL PARAMETERS---

    num_epochs = 2500
    num_worker = 8 # number of threads / cores used
    batch_size = 32
    rates = [0.1, 0.2, 0.3]
    activations = [nn.CELU()]
    num_hidden_layers = [3, 4]

    # loss function
    loss_function = torch.nn.MSELoss()

    #---END MODEL PARAMETERS---
    
    for rate in rates:
        for activation in activations:
            for layers in num_hidden_layers:

                # specify neural network
                model = neural_nets.AHNN(input_size=512, hidden_div=2, output_size=1, num_hidden_layers=layers, activation=activation, dropout_rate=rate)
                architecture = model.get_data()
                
                # specify optimizer
                optimizer = torch.optim.Adam(model.parameters(), lr=0.0005)
                # optimizer = torch.optim.SGD(model.parameters(), lr=0.001)

                # scheduler (this is supposed to reduce the learing rate in every step)
                scheduler = torch.optim.lr_scheduler.LinearLR(optimizer, start_factor=1.0, end_factor=0.1, total_iters=2500)
                #scheduler = None

                model, train_log, val_log = training_counting.train_model(
                    data_train=X_all_test,
                    labels_train=y_all_test,
                    data_val=X_all_val,
                    labels_val=y_all_val,
                    optimizer=optimizer,
                    loss_function=loss_function,
                    scheduler=scheduler,
                    model=model,
                    num_epochs=num_epochs,
                    num_workers=num_worker,
                    batch_size=batch_size)

                # Ensure train_log and val_log are lists of loss values
                train_log = [epoch_loss for epoch_loss in train_log]
                val_log = [epoch_loss for epoch_loss in val_log]


                saver.save_simple(model, train_log, val_log, architecture, activation, optimizer, batch_size, rate, num_epochs)
    
    
                #Visualizer().plot_loss(train_log, val_log, num_epochs, save_path = "./data/counting_spikelets_no_bar_images.png")

   
    #_________________________________________________________________
    #what do change: 
    #dictionary with parameters
    #path to save the model 
    #image path and mappings if different dataset is used 