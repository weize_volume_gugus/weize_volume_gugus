import pytest
import numpy as np

def mat_mult():
    w = np.array([[2,2],[0,2]])
    x = np.array([[2],[2]])
    
    return np.matmul(w,x)

class TestNumpy:
    def test_mat_mult(self):
        z = mat_mult()
        
        assert np.all(z == np.array([[8],[4]]))
